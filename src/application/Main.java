package application;
	
import com.sun.javafx.scene.control.skin.Utils;

import javafx.scene.control.TextField;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/*Nesta actividade vamos criar um login usando o layout gridpane
 * 	- Copiar toda a main no ex8, que j� contem os layouts pretendidos
 * - o LOGIN vai funcionar de duas formas distintas , a partir da regi�o centreal do BorderPane
 *  1� bot�o da regi�o left , ativa um layout com o login
 *  2� botao da regi�o left, ativa um layut com o bot�o centrado. Este bot�o chama o login apartir de um m�todo da utils. Para tal copia-se 
 *  o c�digo do login desenvolvido no 1�  bot�o e cola-se num metodo dessa classe.*/
public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			
			BorderPane layoutRoot = new BorderPane(); // BorderPane, layout de fundo (Root)
			
			//--------------------But�es----------------------------
			Button btnOk = new Button("OK");
			Button btnLogin = new Button("Log-In");
			// top layout
			HBox layoutTop = new HBox(10);			// Cria o layout HBox com 10px espacamento entre objetos
			layoutTop.setPadding(new Insets(15,12,15,12)); //Distancia deste layout ao root layout
			layoutTop.setStyle("-fx-background-color: #336699;");  //Defini��o da cor
					//--3 BOTOES-----
			
			Button btnTopMenu1 = new Button("File");
			Button btnTopMenu2 = new Button("Edit");
			Button btnTopMenu3 = new Button("View");
			
			layoutTop.getChildren().addAll(btnTopMenu1,btnTopMenu2,btnTopMenu3); //Adicionados ao HBox
			
			//leftLayout - Menu							//O mesmo para o VBox
			VBox layoutLeft = new VBox(10);
			layoutLeft.setPadding(new Insets(15,12,15,12));
			layoutLeft.setStyle("-fx-background-color: #336633;");
			
			Button btnLeftMenu1 = new Button("Login Interno");
			Button btnLeftMenu2 = new Button("Login Externo");
			Button btnLeftMenu3 = new Button("op��o3");
			layoutLeft.getChildren().addAll(btnLeftMenu1,btnLeftMenu2,btnLeftMenu3);
			
			//layout center m�todo 1
			GridPane layoutCenter = new GridPane(); 
			layoutCenter.setPadding(new Insets(50,50,50,50));
			
			//layout center VBox metodo 2
			VBox layoutCenterVBox = new VBox(10);
			layoutCenterVBox.setPadding(new Insets(100,100,1000,100));
			layoutCenterVBox.setStyle("-fx-background-color: #D00000;");
			layoutCenterVBox.getChildren().addAll(btnLogin);
			
			//Labels
			Label labelUserName = new Label("Username: ");
			Label labelPassword = new Label("Password: ");
			//TextFields
			TextField textFieldUserName = new TextField();
			PasswordField passwordFieldPassword = new PasswordField();
			
			
			//Adicionar � Grid os botoes, label e textfields-----------------------------
			layoutCenter.add(labelUserName, 0, 0);
			layoutCenter.add(textFieldUserName, 1, 0);
			layoutCenter.add(labelPassword, 0, 2);
			layoutCenter.add(passwordFieldPassword, 1,2);
			layoutCenter.add(btnOk, 1, 3);
			
			
			//Eventos
			btnLeftMenu1.setOnAction(e-> layoutRoot.setCenter(layoutCenter));
			btnLeftMenu2.setOnAction(e-> layoutRoot.setCenter(layoutCenterVBox));
			btnLogin.setOnAction(e->application.Utils.login() );
			//btnLeftMenu2.setOnAction(e);
			//Adicionar os layouts anteriores, j� montados, �s regi�es do BorderPane
			
			
			layoutRoot.setTop(layoutTop);
			layoutRoot.setLeft(layoutLeft);
			
			Scene scene = new Scene(layoutRoot,500,500);
		
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}